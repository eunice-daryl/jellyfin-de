# Jellyfin-DE

## Introduction

L'objectif principalle du projet Jellyfin-DE a ete de déploie un serveur multimédia open-source sécurisé sur une machine virtuelle VM-Azure hébergée à distance sur Microsoft Azure permettent ainsi d'offrir aux utilisateurs une expérience sécurisée tout en préservant la confidentialité et l'intégrité de leurs données multimédias.

## Explication et fonctionnement 

Après la connexion à la machine virtuelle via une clé SSH sécurisée, la configuration distante est établie. Ce document fournit un aperçu des différentes commandes utilisées pour renforcer la sécurité de la VM et du site. Au cours du projet, plusieurs solutions et techniques ont été élaborées pour sécuriser le site et la VM.

### Caractéristiques de la VM :

- VPN pour une connexion sécurisée.
- Certificat SSL/TLS pour le chiffrement des données.
- Pare-feu pour contrôler le trafic réseau.
- IP publique et IP privée pour une connectivité réseau.

**_Autres mesures de sécurité détaillées dans le rapport complet au bas de la page_**.

### Sécurisation du site :

Après l'hébergement du site, une interface administrateur a été mise en place. Cette interface dispose d'un tableau de bord permettant de configurer le site en temps réel, d'effectuer des modifications et des réglages directement. Elle permet également de gérer les différents accès de manière sécurisée.

## Fonctionnalités de l'interface administrateur :

- Authentification forte pour renforcer la sécurité.
- Gestion des utilisateurs inscrits sur le site.
- Limite d'accès aux médias disponibles.
- Restrictions d'accès aux configurations sensibles.
- Module de notification pour les mises à jour des comptes utilisateurs ou modification aporter.

Ce document vise à fournir un aperçu des mesures de sécurité mises en place pour la VM et le site. Il est recommandé de suivre attentivement les instructions fournies dans le rapport complet pour une mise en œuvre réussie de ces mesures de sécurité.

Pour plus de détails sur la configuration et le fonctionnement de chaque élément de sécurité, veuillez vous reporter au rapport complet fourni dans le dossier associé.


## Accès au site

Entrer dans un Navigateur  : http://jellyfin-de.ovh:8096

login: 
 **Utilisateur** : Test

 **Mot de Passe** : test

Après la connexion, il est possible de changer le mot de passe de l'utilisateur

 http://jellyfin-de.ovh:8096/web/index.html#!/myprofile.html?userId=cb422d80a9b14202a983d0476ee792e8

<img src="image.png" alt="Texte alternatif" style="width: 22px; height: auto;"> 

**Vous pouvez visiter le site ! seulement lorque la vm est en ligne**

 Laissez-nous un message sur ce lien pour activer la VM en cas de besoin de vérification potentielle.

 Pour toute question ou préoccupation, contactez-nous par email sur :

  [geunice.sombo@gmail.com](mailto:geunice.sombo@gmail.com)

  **ou**

  [tatoudaryl@gmail.com](mailto:tatoudaryl@gmail.com)
 

 Pour ce qui est des étapes de configuration du site hébergé, téléchargez le rapport sur le site Web sous-jacent pour obtenir davantage d'informations
 
 [Dowload Rappot Jellyfin-DE](<Projet _ Jellyfin-DE.docx>)
 
 Bonne analyse et lecture.

 **Présentation**

 Vous pouvez consulter les diapositives de présentation du projet Jellyfin-DE en suivant ce lien :

 [Diapositives Jellyfin-DE](<diapo_Jellyfin-de.pdf>)

 ## Remerciment

 Nous tenons à exprimer nos sincères remerciements à nos professeurs pour nous avoir confié ce projet d'infrastructure. Leur guidance et leur soutien ont été essentiels dans notre parcours d'apprentissage, et nous sommes reconnaissants de cette opportunité de mettre en pratique nos connaissances.


