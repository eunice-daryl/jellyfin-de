# La sécurisation du Site WEB jellyfin


 #   JELLYFIN-DE


 ## Resultat Commande 

 - **VM distant Azure**

## CONNEXION SSH  
```
PS C:\Users\Usager> ssh Jellyfin-DE@98.66.169.29
Welcome to Ubuntu 20.04.6 LTS (GNU/Linux 5.15.0-1059-azure x86_64)

(.......)
Last login: Tue Apr  2 13:57:10 2024 from 195.7.117.146

```
- **hebergement du site web**

- ##  Configuration

- **Instalation**


- l'installation et la gestion de logiciels sur le système, ainsi que la sécurité et la gestion des clés

```
Jellyfin-DE@Jellyfin-DE:~$ sudo apt install curl gnupg
Reading package lists... Done
Building dependency tree
(.......)
```

- "universe" offre une variété de logiciels maintenus par la communauté Ubuntu.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo apt update
Jellyfin-DE@Jellyfin-DE:~$ sudo add-apt-repository universe
'universe' distribution component is already enabled for all sources.
Jellyfin-DE@Jellyfin-DE:~$
```

- Création d'un répertoire sécurisé pour les clés d'authentification, ajout d'une clé GPG Jellyfin.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo mkdir -p /etc/apt/keyrings
Jellyfin-DE@Jellyfin-DE:~$ curl -fsSL https://repo.jellyfin.org/jellyfin_team.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/jellyfin.gpg
File '/etc/apt/keyrings/jellyfin.gpg' exists. Overwrite? (y/N) y
Jellyfin-DE@Jellyfin-DE:~$ echo $?
0
```
--------------
-  es commandes visent à configurer les sources de paquets Apt afin de faciliter l'installation ou la mise à jour du logiciel Jellyfin sur le système.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo nano /etc/apt/sources.list.d/jellyfin.source
s
Jellyfin-DE@Jellyfin-DE:~$ sudo cat /etc/apt/sources.list.d/jellyfin.sources
export VERSION_OS="$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release )"
export VERSION_CODENAME="$( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release )"
export DPKG_ARCHITECTURE="$( dpkg --print-architecture )"
cat <<EOF | sudo tee /etc/apt/sources.list.d/jellyfin.sources
Types: deb
URIs: https://repo.jellyfin.org/${VERSION_OS}
Suites: ${VERSION_CODENAME}
Components: main
Architectures: ${DPKG_ARCHITECTURE}
Signed-By: /etc/apt/keyrings/jellyfin.gpg
EOF


```


- Démarrage du service Jellyfin sur la machine virtuelle Azure

```
Jellyfin-DE@Jellyfin-DE:~$ sudo systemctl start jellyfin
Jellyfin-DE@Jellyfin-DE:~$ sudo service jellyfin start

```

- **Configuration Web**

-  Configurer paramètres serveur Jellyfin

```
Jellyfin-DE@Jellyfin-DE:~$ sudo nano /etc/jellyfin/server.xml
Jellyfin-DE@Jellyfin-DE:~$ sudo cat /etc/jellyfin/server.xml
<ServerConfiguration>
    <WebClient>
        <BaseUrl>http://98.66.169.29:8096</BaseUrl>
        <UpdateChannel>release</UpdateChannel>
        <UpdatePolicy>default</UpdatePolicy>
        <Path>C:\Users\Usager\Downloads\Music-T</Path>
    </WebClient>
</ServerConfiguration>
Jellyfin-DE@Jellyfin-DE:~$
```

- Vérification du pare-feu et du routeur pour le port 8096 vers le serveur Jellyfin.
```
Jellyfin-DE@Jellyfin-DE:~$ sudo ufw allow 8096/tcp
Rules updated
Rules updated (v6)
```


-  Liste des dossiers créés par Jellyfin pour accueillir les informations à transmettre au client.

```
Jellyfin-DE@Jellyfin-DE:~$ ls
Clip-Music  Film  Music  Shows
```


- info disque et redémarrage du service Jellyfin sur la VM.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo systemctl restart jellyfin
Jellyfin-DE@Jellyfin-DE:~$ sudo service jellyfin start
Jellyfin-DE@Jellyfin-DE:~$ sudo fdisk -l
Disk /dev/loop0: 63.93 MiB,(...)

Disk /dev/loop1: 91.85 MiB, (...)

Disk /dev/loop2: 39.1 MiB,(...)

Disk /dev/sda: 30 GiB,(....)

Disk /dev/sdb: 4 GiB, (.....)

```
 - Informations sur les caractéristiques des périphériques de stockage, utiles pour faciliter la gestion, la configuration et le dépannage du stockage système.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo blkid
/dev/sda1: LABEL="cloudimg-rootfs" UUID="fb8d1de5-d160-4ed5-97d5-976e1ec09b1f" TYPE="ext4" PARTUUID="b787777b-8807-4aee-8856-0dcf8a637ed2"
/dev/sda15: LABEL_FATBOOT="UEFI" LABEL="UEFI" UUID="7100-7162" TYPE="vfat" PARTUUID="4cd7904e-2463-4522-8d77-a23f6f19edcc"
/dev/sdb1: UUID="44ee9a01-5503-4582-acab-992d516f6332" TYPE="ext4" PARTUUID="83375fdd-01"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/sda14: PARTUUID="20595b0e-62b7-4e72-b0e6-4620eb5d4703"
```

- Configuration du gestionnaire de session.

```
Jellyfin-DE@Jellyfin-DE:~$ sudo cat /etc/systemd/logind.conf
#  This file is part of systemd.
(#.....)

[Login]
#NAutoVTs=6
#ReserveVT=6
(....)
#InhibitDelayMaxSec=5
#HandlePowerKey=poweroff
HandleSuspendKey=ignore
#HandleHibernateKey=hibernate
HandleLidSwitch=ignore
#HandleLidSwitchExternalPower=suspend
HandleLidSwitchDocked=ignore
#PowerKeyIgnoreInhibited=no
(.......)

Jellyfin-DE@Jellyfin-DE:~$ sudo systemctl restart systemd-logind

```
--------------------- Configuration Firewall---------------------

## Firewall

- Activation Pare-Feu

```
Jellyfin-DE@Jellyfin-DE:~$ sudo ufw status
Status: inactive
Jellyfin-DE@Jellyfin-DE:~$ sudo ufw enable
Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
Firewall is active and enabled on system startup
Jellyfin-DE@Jellyfin-DE:~$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
8096/tcp                   ALLOW       Anywhere
8096/tcp (v6)              ALLOW       Anywhere (v6)

```


## Générer un certificat SSL avec Let's Encrypt 

```
Jellyfin-DE@Jellyfin-DE:~$ sudo apt update
(..)
Jellyfin-DE@Jellyfin-DE:~$ sudo apt install certbot
Reading package lists... Done
(......)
Created symlink /etc/systemd/system/timers.target.wants/certbot.timer → /lib/systemd/system/certbot.timer.
Processing triggers for man-db (2.9.1-1) ...

```
- Nouveau  certificat généré par OVH.

![alt text](certificatSSL.png)


# DNS

- Pour empêche les piratages et les faux en garantissant que les informations de nommage sont authentiques et non altérées grâce à des méthodes comme DNSSEC et des listes de blocage pour protéger contre les menaces.

![alt text](<Screenshot 2024-04-22 050427.png>)

![alt text](image.png)


 **Vérification**

![alt text](<Screenshot 2024-04-22 052124.png>)



- Analyse et  diagnostic des problèmes de DNS, vérification de la résolution DNS et l'obtention d'informations détaillées sur les configurations DNS d'un domaine.

*voir* 

https://www.nslookup.io/domains/jellyfin-de.ovh/dns-records/#cloudflare


----------------------mise en plase DataBase------------------

## Firebase
- instalation package 

```
Jellyfin-DE@Jellyfin-DE:~$ sudo apt update
Hit:1 http://azure.archive.ubuntu.com/ubuntu focal InRelease
Hit:2 http://azure.archive.ubuntu.com/ubuntu focal-updates InRelease
(.....)
Jellyfin-DE@Jellyfin-DE:~$ sudo apt install nodejs npm
Reading package lists... Done
(.....)

```
- Ces commandes permettent d'installer les outils Firebase sur le système, ce qui inclut l'initialisation d'une application Firebase via un script JavaScript et l'installation des outils Firebase globalement à l'aide de npm
```

Jellyfin-DE@Jellyfin-DE:~$ sudo cat index.html

<script type="module">
  
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.10.0/firebase-app.js";
  import { getAnalytics } from "https://www.gstatic.com/firebasejs/10.10.0/firebase-analytics.js";
 
  // https://firebase.google.com/docs/web/setup#available-libraries
  const firebaseConfig = {
    apiKey: "AIzaSyCTX21A9kFGyfPpV_WnuHRx_e7N56IEBhU",
    authDomain: "jellyfin-de.firebaseapp.com",
    projectId: "jellyfin-de",
    storageBucket: "jellyfin-de.appspot.com",
    messagingSenderId: "1060560976573",
    appId: "1:1060560976573:web:2983973127b337f59ad5d4",
    measurementId: "G-Z3Z1YVK7KQ"
  };
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
</script>

Jellyfin-DE@Jellyfin-DE:~$ npm install -g firebase-tools

```

